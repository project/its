(function ($) {
  Drupal.theme.itsContentChangedWarning = function () {
    return '<div class="messages warning">' + Drupal.theme('tableDragChangedMarker') + ' ' + Drupal.t('The changes to the content will not be saved until the <em>Save</em> button is clicked.') + '</div>';
  };

  if (!Drupal.its) {
    Drupal.its = {
    };
  }
  if (!Drupal.its.settings) {
    Drupal.its.settings = {
    };
  }

  Drupal.its.filter = {

    selectorMap: {
      its_translate: ':translate(no)',
      its_locale: ':localeFilter',
      its_textAnalysis: ':textAnalysis',
      its_locNote: ':locNote',
      its_lang: ':lang',
      its_dir: ':dir',
      its_terminology: ':terminology'
    },
    activeCategories: [],

    /**
     * Return whether the passed node belongs to this plugin.
     *
     * @param node
     *   The currently focused DOM element in the editor content.
     * @param meta_name
     *   Name of the meta_data to process
     */
    isNode: function (node, meta_name) {
      var el = (node.jquery) ? node : $(node);
      return el.is(this.selectorMap[meta_name]);
    },

    /**
     * Highlight a context and add a tooltip.
     *
     * @param meta_name
     * @param context
     */
    show: function (meta_name, context) {
      var filter = this;
      if (filter.activeCategories.indexOf(meta_name) != -1) {
        return;
      }
      var selector = filter.selectorMap[meta_name]
      if (context.jquery && context.is(selector)) {
        filter.addHighlightCSS(meta_name, context);
      }
      $(selector, context).each(function(key, element) {
        filter.addHighlightCSS(meta_name, element);
      });
    },

    addHighlightCSS: function(meta_name, element) {
      var filter = this;
      var $element = (element.jquery) ? element : $(element);
      $element.addClass(meta_name);
      filter.refreshToolTip($element, 'add', meta_name);
    },

    removeHighlightCSS: function(meta_name, element) {
      var filter = this;
      var $element = (element.jquery) ? element : $(element);
      $element.removeClass(meta_name);
      filter.refreshToolTip($element, 'remove', meta_name);
      filter.removeSelectClassAndEmptySpan($element)
    },

    /**
     * Remove the color highlight of the elements.
     *
     * @param meta_name
     * @param context
     * @param selector
     */
    hide:function (meta_name, context) {
      var filter = this;
      if (filter.activeCategories.indexOf(meta_name) == -1) {
        return;
      }
      if (context.jquery && context.hasClass(meta_name)) {
        filter.removeHighlightCSS(meta_name, context);
      }
      $('.' + meta_name, context).each(function(key, element) {
        filter.removeHighlightCSS(meta_name, element);
      });
    },

    /**
     * Refresh the tooltip of the selected element.
     * @param $element
     * @param remove name of a data category to remove
     */
    refreshToolTip: function(element, action, meta_name) {
      var $elements = (element.jquery) ? element : $(element),
        filter = this;
      // simpletip not added to js
      if (!$elements.simpletip)
        return;
      $elements.each(function(k, element) {
        var $element = (element.jquery) ? element : $(element);
        var active_meta = [];
        $.each(filter.activeCategories, function(meta_name) {
          active_meta.push(meta_name);
        });
        if (action == 'remove') {
          var index = active_meta.indexOf(meta_name);
          if (index > -1) {
            delete active_meta[index];
          }
        }
        else if (action == 'add') {
          active_meta.push(meta_name);
        }
        var tooltip = '';
        var itsData = $element.getITSData();

        if (active_meta.indexOf('its_translate') > -1 && !itsData.translate) {
          if (tooltip !== '') {
            tooltip += '<br/>';
          }
          tooltip += Drupal.t('This part will not be translated');
        }
        if (active_meta.indexOf('its_locNote') > -1 && itsData.locNote) {
          if (tooltip !== '') {
            tooltip += '<br/>';
          }
          tooltip += 'locNote=' + itsData.locNote;
          if (itsData.locNoteType) {
            tooltip += '<br/>';
            tooltip += 'locNoteType=' + itsData.locNoteType;
          }
        }

        if (active_meta.indexOf('its_locale') > -1 && itsData.localeFilter) {
          if (tooltip !== '') {
            tooltip += '<br/>';
          }
          tooltip += 'localeFilterList=' + itsData.localeFilter;
        }

        if (active_meta.indexOf('its_lang') > -1 && itsData.lang && itsData.lang != Drupal.settings.its.language) {
          if (tooltip !== '') {
            tooltip += '<br/>';
          }
          tooltip += 'lang=' + itsData.lang;
        }

        if (active_meta.indexOf('its_dir') > -1 && itsData.dir && itsData.dir != Drupal.settings.its.dir) {
          if (tooltip !== '') {
            tooltip += '<br/>';
          }
          tooltip += 'dir=' + itsData.dir;
        }

        if (active_meta.indexOf('its_terminology') > -1 && itsData.term) {
          if (tooltip !== '') {
            tooltip += '<br/>';
          }
          tooltip += 'term=' + (itsData.term ? 'yes' : 'no');
          if (itsData.termInfoRef) {
            tooltip += '<br/>' + 'termInfoRef=' + itsData.termInfoRef;
          }
          if (itsData.termConfidence) {
            tooltip += '<br/>' + 'termConfidence=' + itsData.termConfidence;
          }
        }

        if (active_meta.indexOf('its_textAnalysis') > -1 && (itsData.taConfidence || itsData.taClassRef || itsData.taSource || itsData.taIdent || itsData.taIdentRef)) {
          var attributes = ['taConfidence', 'taClassRef', 'taSource', 'taIdent', 'taIdentRef'];
          $.each(attributes, function(k, attribute) {
            if (itsData[attribute]) {
              if (tooltip !== '') {
                tooltip += '<br/>';
              }
              tooltip += attribute + '=' + itsData[attribute];
            }
          });
        }

        var simpleTip = $element.data('simpletip');
        if (tooltip !== '') {
          if (typeof simpleTip == 'undefined') {
            var pos = $element.position(),
              elOffset = $element.offset(),
              offset = [pos.left - elOffset.left + 5, pos.top - elOffset.top + 5];
            $element.simpletip({content: tooltip, fixed: true, position: 'bottom', offset: offset});
            simpleTip = $element.data('simpletip');
            simpleTip.getTooltip().appendTo('#its-tooltip-container');
          }
          else {
            simpleTip.update(tooltip);
          }
        }
        else {
          if (simpleTip) {
            simpleTip.getTooltip().remove();
            $element.removeData('simpletip');
          }
        }
      });
    },

    /**
     * Add meta data to the selected content.
     *
     * @param event
     * @param meta_name
     */
    add: function (event, meta_name) {
      var elements = $('.field-its-edit-text').wrapSelection();
      // wrap selections adds a span, this messes up saved xpaths
      $.clearITSCache();
      if (elements.length > 0)
        this.defineITSData(meta_name, elements);
    },

    /**
     * Depending on type open a modal to input the ITS data and set this data or set it directly (only translate).
     *
     * @param type string
     *   One of translate|locNote|stroageSize|allowedCharacters.
     *
     * @param $element
     *   jQuery element which is selected.
     *
     * @param cb
     *   Callback function, after inserting or closing
     */
    defineITSData: function(type, $element, cb) {
      var modal = false,
        edit = false,
        global = true,
        itsData = $element.getITSData(),
        width = 350;

      switch (type) {
        case 'its_translate':
        default:
          var action = 'close';
          $element.each(function(k, subElement) {
            var $subElement = $(subElement);
            itsData = $subElement.getITSData();
            if (!itsData.translate) {
              action = 'remove';
              // No global rules.
              if (subElement.hasAttribute('translate')) {
                $subElement.removeAttr('translate');
              }
              // This is set by a global rule, so force to translate.
              else {
                // FF, IE, ..
                if (typeof subElement.translate === 'undefined') {
                  $subElement.attr('translate', 'yes');
                }
                // Chrome
                else {
                  $subElement.attr('translate', true);
                }
              }
            }
            else {
              action = 'add';
              // Its translate='yes', so it seems there is a global rule, for it, so only remove this attribute.
              if (subElement.hasAttribute('translate') && ($subElement.attr('translate') === 'yes' || $subElement.attr('translate') === true)) {
                $subElement.removeAttr('translate');
              }
              // No global rules.
              else {
                // FF, IE, ..
                if (typeof subElement.translate === 'undefined') {
                  $subElement.attr('translate', 'no');
                }
                // Chrome
                else {
                  $subElement.attr('translate', false);
                }
              }
            }
          });
          this.finishEditing(false, type, $element, action);
          this.removeSelectClassAndEmptySpan($element);
          if (cb && typeof cb == 'function') {
            cb(type, action, $element);
          }
          break;

        case 'its_locNote':
          modal = $('<div id="edit-form" title="' + Drupal.t('Further annotations') + '">' +
            '<label for="edit-form-locNoteType">' + Drupal.t('Type') + '</label><select id="edit-form-locNoteType" class="value"><option value="description">' + Drupal.t('description') + '</option><option value="alert">' + Drupal.t('alert') + '</option></select><br>' +
            '<label for="edit-form-locNote">' + Drupal.t('Note') + '</label><textarea id="edit-form-locNote" class="value" cols="5"></textarea>' +
            '</div>');
          if (itsData.locNote) {
            edit = true;
            if ($element.attr('its-loc-note')) {
              global = false;
            }
          }
          break;

        case 'its_locale':
          modal = $('<div id="edit-form" title="' + Drupal.t('Market-specific text') + '">' +
            '<label for="edit-form-localeFilterList">' + Drupal.t('Filter') + '</label><input id="edit-form-localeFilterList" class="value" type="value" /><br>' +
            Drupal.t('Comma separated list of languages this should be included.') +
            '</div>');
          if (itsData.localeFilterList) {
            edit = true;
            if ($element.attr('its-locale-filter-list')) {
              global = false;
            }
          }
          break;

        case 'its_lang':
          modal = $('<div id="edit-form" title="' + Drupal.t('Language') + '">' +
            '<label for="edit-form-lang">' + Drupal.t('Language') + '</label><input id="edit-form-lang" class="value" type="value" /><br>' +
            Drupal.t('Define in which language the selected content is.') +
            '</div>');
          if (itsData.lang) {
            edit = true;
            if ($element.attr('lang')) {
              global = false;
            }
          }
          break;

        case 'its_textAnalysis':
          modal = $('<div id="edit-form" title="' + Drupal.t('Text analysis') + '">' +
            '<label for="edit-form-taConfidence">' + Drupal.t('Confidence') + '</label><input id="edit-form-taConfidence" class="value" type="value" /><br>' +
            '<label for="edit-form-taClassRef">' + Drupal.t('Class Reference') + '</label><input id="edit-form-taClassRef" class="value" type="value" /><br>' +
            Drupal.t('One of the following:') + ' <br>' +
            '<label for="edit-form-taSource">' + Drupal.t('Source') + '</label><input id="edit-form-taSource" class="value" type="value" /><br>' +
            '<label for="edit-form-taIdent">' + Drupal.t('Identifier') + '</label><input id="edit-form-taIdent" class="value" type="value" /><br>' +
            ' - ' + Drupal.t('or') + ' -<br>' +
            '<label for="edit-form-taIdentRef">' + Drupal.t('Identifier Reference') + '</label><input id="edit-form-taIdentRef" class="value" type="value" /><br>' +
            '<label for="edit-form-annotatorsRef-text-analysis">' + Drupal.t('Annotators Reference') + '</label><input id="edit-form-annotatorsRef" class="value" type="value" disabled="disabled"/><br>' +
            '</div>');
          if (itsData.taConfidence || itsData.taClassRef || itsData.taSource || itsData.taIdent || itsData.taIdentRef) {
            edit = true;
          }
          // Only local available
          global = false;
          break;

        case 'its_dir':
          modal = $('<div id="edit-form" title="' + Drupal.t('Directionality') + '">' +
            '<label for="edit-form-dir">' + Drupal.t('Directionality') + '</label><select id="edit-form-dir" class="value"><option value="ltr">' + Drupal.t('Left-To-Right') + '</option><option value="rtl">' + Drupal.t('Right-To-Left') + '</option></select><br>' +
            Drupal.t('Define in which read direction the selected content is.') +
            '</div>');
          if (itsData.dir) {
            edit = true;
            if ($element.attr('dir')) {
              global = false;
            }
          }
          break;

        case 'its_terminology':
          modal = $('<div id="edit-form" title="' + Drupal.t('Terminology') + '">' +
            '<label for="edit-form-term">' + Drupal.t('Confidence') + '</label><select id="edit-form-term" class="value"><option value="yes">' + Drupal.t('Yes') + '</option><option value="no">' + Drupal.t('No') + '</option></select><br>' +
            '<label for="edit-form-termInfoRef">' + Drupal.t('Source') + '</label><input id="edit-form-termInfoRef" class="value" type="value" /><br>' +
            '<label for="edit-form-termConfidence">' + Drupal.t('Class Reference') + '</label><input id="edit-form-termConfidence" class="value" type="value" /><br>' +
            '<label for="edit-form-annotatorsRef-terminology">' + Drupal.t('Annotators Reference') + '</label><input id="edit-form-annotatorsRef" class="value" type="value" disabled="disabled"/><br>' +
            '</div>');
          if (itsData.term || itsData.termConfidence || itsData.termInfoRef) {
            edit = true;
          }
          if (itsData.term) {
            itsData.term = 'yes';
          }
          else {
            itsData.term = 'no';
          }
          // Only local available
          global = false;
          break;

      }

      var buttons = {},
        filter = this;

      if (modal) {
        modal.hide();
        $('body').append(modal);
        modal.dialog({
          autoOpen:false,
          modal:true,
          close: function(event, ui) {
            filter.removeSelectClassAndEmptySpan($element);
            if (cb && typeof cb == 'function') {
              cb(type, 'close', $element);
            }
          }
        });
        modal.addClass('its-edit-local-form');
        modal.addClass('its-edit-local-form-' + type)
        if (width) {
          modal.dialog('option', 'width', width);
        }

        $('.value', modal).each(function() {
          $(this).val('');
        });
        $.each(itsData, function(key, value) {
          if (key === 'annotatorsRef') {
            $.each(value, function(key, value) {
              var inputField = $('#edit-form-' + key + '-' + key, modal);
              if (inputField.length != 0) {
                inputField.val(value);
              }
            });
          }
          else {
            var inputField = $('#edit-form-' + key, modal);
            if (inputField.length != 0) {
              inputField.val(value);
            }
          }
        });

        if (edit) {
          buttons[Drupal.t('edit')] = function () {
            $.each(filter.getDataFromModal(modal), function(attrName, value) {
              if (value === '') {
                $element.removeAttr(attrName);
              }
              else {
                $element.attr(attrName, value);
              }
            });
            filter.finishEditing(this, type, $element, 'edit', cb);
          };
          if (!global) {
            buttons[Drupal.t('remove')] = function () {
              $.each(filter.getDataFromModal(modal), function(attrName, value) {
                $element.removeAttr(attrName);
              });
              filter.finishEditing(this, type, $element, 'remove', cb);
            }
          }
        }
        else {
          buttons[Drupal.t('add')] = function () {
            $.each(filter.getDataFromModal(modal), function(attrName, value) {
              if (value === '') {
                $element.removeAttr(attrName);
              }
              else {
                $element.attr(attrName, value);
              }
            });
            filter.finishEditing(this, type, $element, 'add', cb);
          };
        }
        buttons[Drupal.t('close')] = function () {
          filter.finishEditing(this, type, $element, 'close', cb);
        };

        modal.dialog('option', 'buttons', buttons);
        modal.dialog('open');

      }
    },

    /**
     * Standard function after editing a element.
     *
     * Close modal and remove it. Update the highlight class and tooltip of the element.
     * Remove empty spans.
     *
     * @param modal
     * @param type
     * @param $element
     * @param action (add, edit, remove, close)
     * @param cb callback function
     */
    finishEditing: function finishEditing(modal, type, $element, action, cb) {
      // Refresh the highlight of this element, if the checkbox is checked
      if (Drupal.its.filter.activeCategories.indexOf(type) !== -1) {
        if (action === 'remove') {
          this.removeHighlightCSS(type, $element);
        }
        else if (action === 'add' || action === 'edit') {
          this.addHighlightCSS(type, $element);
        }
      }

      if ($('#edit-metadata + .messages.warning').length == 0) {
        $(Drupal.theme('itsContentChangedWarning')).insertAfter('#edit-metadata').hide().fadeIn('slow');
      }

      if (modal) {
        $(modal).dialog('close').remove();
      }

      if (cb && typeof cb == 'function') {
        cb(type, action, $element);
      }
    },

    /**
     * Remove the select class from wrapSelection and remove spans without attributes.
     * @param $element
     */
    removeSelectClassAndEmptySpan: function($element) {
      // Remove the wrap selector class and remove empty spans and class attributes.
      if ($element.selector.indexOf('.sel_') === 0) {
        $element.removeClass($element.selector.substr(1));
      }

      // Remove empty spans
      $element.each(function(key, subElement) {
        var $subElement = $(subElement);
        // Even when every class is removed, the empty class attribute still exists.
        if ($subElement.attr('class') === '') {
          $subElement.removeAttr('class');
        }
        if (subElement.nodeName.toLowerCase() == 'span' && subElement.attributes.length == 0) {
          $subElement.replaceWith($subElement.html());
        }
      });
    },

    /**
     * Get the input data from a open modal.
     *
     * @param modal
     *
     * @return {Object}
     *   key is the its attribute name and value the input data
     */
    getDataFromModal: function (modal) {
      var data = {};
      $('.value', modal).each(function() {
        var $this = $(this),
          value = $this.val(),
          id = $this.attr('id');
        id = id.replace(/edit-form-/, '');
        id = id.replace(/([A-Z])/g, '-$1').toLowerCase();
        if (id !== 'lang' && id !== 'dir') {
          id = 'its-' + id;
        }
        data[id] = value;
      });
      return data;
    }
  };

  Drupal.behaviors.its_text = {

    context: null,
    metadataToolOverlay: null,

    metadataFieldset: null,
    metadataPosition: {},
    stickyMetaDataFieldSet: false,
    metadataFieldsetWidth: 0,
    metadataFieldsetTop: 10,

    translateRuleController: null,
    translateRuleKeyMap: {},

    attach:function (context, settings) {
      var that = this;
      if ('its' in settings) {
        Drupal.its.settings = settings.its;
        if (Drupal.settings.its.language)
          Drupal.its.filter.selectorMap['its_lang'] = ':lang(!' + Drupal.settings.its.language + ')'
      }
      $.parseITS(function(rulesController) {
        $.each(rulesController.supportedRules, function(key, rule) {
          if (rule.NAME === 'translate') {
            that.translateRuleController = rule;
            that.context = context;
            $('#its-translate-values').find('.form-wrapper').each(that.setGlobalTranslateRule);
          }
        });
        var its = $('html').getITSData();
        Drupal.its.filter.selectorMap['its_dir'] = ':dir(!' + its.dir + ')';
        if (!Drupal.settings.its) {
          Drupal.settings.its = {};
        }
        Drupal.settings.its.dir = its.dir;
      });

      if ($('.meta-data input.form-checkbox:not(.its-processed)').size() == 0) {
        Drupal.its.filter.activeCategories = [];
        $.each(Drupal.its.filter.selectorMap, function(key, selector) {
          Drupal.its.filter.activeCategories.push(key);
        });
      }
      $('.meta-data input.form-checkbox:not(.its-processed)', context).each(function () {
        $(this).addClass('its-processed');
        $(this).click(function () {
          that.toggleFilter(this, context);
        });
        that.toggleFilter(this, context);
        $('.field-item a', context).click(function (event) {
          event.preventDefault();
        });
        $(this).next().click(function (event) {
          event.stopPropagation();
          event.preventDefault();
          var meta_name = $(this).children('a').attr('href').substring(1);
          Drupal.its.filter.add(event, meta_name);
        });
      });

      that.metadataToolOverlay = $('#metadata_overlay:not(.its-processed)');
      if (that.metadataToolOverlay.length > 0) {
        that.metadataToolOverlay.addClass('its-processed');
        that.metadataToolOverlay.appendTo('body');
        that.metadataToolOverlay.hide();
        $('.field-its-edit-text').bind("mouseup", function(event){
          var range = $('.field-its-edit-text').getRangeAt();
          if (range) {
            $('a', that.metadataToolOverlay).removeClass('active');
            var nodes = range.GetContainedNodes();
            $.each(Drupal.its.filter.selectorMap, function(meta, selector) {
              var link = $('a[href="#' + meta + '"]', that.metadataToolOverlay);
              for (var i=0;i<nodes.length;i++) {
                for (var j=0;j<nodes[i].length;j++) {
                  if (nodes[i][j].nodeType === 3) {
                    var parent = nodes[i][j].parentNode;
                    if (parent.childNodes.length === nodes[i].length) {
                      if ($(parent).is(selector)) {
                        link.addClass('active')
                      }
                      else {
                        link.removeClass('active')
                      }
                      break;
                    }
                  }
                  else {
                    if ($(nodes[i][j]).is(selector)) {
                      link.addClass('active')
                    }
                    else {
                      link.removeClass('active')
                    }
                  }
                }
              }
            });
            that.metadataToolOverlay.show();
            that.metadataToolOverlay.css({top: event.pageY - 57, left: event.pageX + 10});
          }
          else {
            that.metadataToolOverlay.hide();
          }
        });

        $('a', that.metadataToolOverlay).each(function() {
          var link = $(this),
            data = link.data(),
            meta_name = $(this).attr('href').substring(1);
          if (data['key-map']) {
            $('body').bind('keyup', function(event) {
              if (event.target.nodeName != 'INPUT' && event.target.nodeName != 'TEXTAREA' && !event.shiftKey && !event.altKey && event.which && that.metadataToolOverlay.is(':visible')) {
                if (String.fromCharCode(event.which).toLowerCase() === data['key-map']) {
                  Drupal.its.filter.add(event, meta_name);
                  that.metadataToolOverlay.hide();
                }
              }
            })
          }
          link.click(function(event) {
            event.stopPropagation();
            event.preventDefault();
            Drupal.its.filter.add(event, meta_name);
            that.metadataToolOverlay.hide();
          });
        });
      }

      $('#its-lm-form').submit(function() {
        // Remove all the highlighting markup.
        $(Drupal.its.filter.activeCategories).each(function (key, meta_name) {
          $('.field-its-edit-text', context).each(function () {
            Drupal.its.filter.hide(meta_name, this);
          });
        });
        // Set the changed input to the textareas.
        $('.field-its-edit-text', context).each(function(key, element) {
          var $element = $(element),
            id = $element.parent().attr('id');
          id = '#' + id.substr(0, id.indexOf("-view"));
          $(id).text($element.html());
        });

        return true;
      });

      that.metadataFieldset = $('#edit-metadata');
      if (that.metadataFieldset.size() > 0) {
        that.metadataPosition = that.metadataFieldset.position();
        that.metadataFieldsetWidth = that.metadataFieldset.width();
        that.metadataPosition.top += that.metadataFieldset.height() + 20;
        if (window.parent.Drupal.toolbar) {
          that.metadataFieldsetTop = window.parent.Drupal.toolbar.height();
        }

        $('fieldset.collapsible').bind('collapsed', function() {
          var fieldset = $(this),
            oldHeight = fieldset.height();
          setTimeout(function() {
            var height = fieldset.height();
            if (height > 0) {
              that.metadataPosition.top += height;
            }
            else {
              that.metadataPosition.top -= oldHeight;
            }

            if (window.parent.Drupal.toolbar) {
              that.metadataFieldsetTop = window.parent.Drupal.toolbar.height();
            }
            that.refreshStickTable();
          }, 500)
        });

        $(window).scroll(this.refreshStickTable);
      }
    },

    refreshStickTable: function() {
      var self = Drupal.behaviors.its_text;
      if ($(window).scrollTop() > self.metadataPosition.top) {
        if (!self.stickyMetaDataFieldSet) {
          self.stickyMetaDataFieldSet = true;
          self.metadataFieldset.css({position: 'fixed', top: self.metadataFieldsetTop, 'background-color': 'white', width: self.metadataFieldsetWidth});
        }
      }
      else if (self.stickyMetaDataFieldSet) {
        self.stickyMetaDataFieldSet = false;
        self.metadataFieldset.removeAttr('style');
      }
    },

    toggleFilter: function (checkbox, context) {
      var meta_name = $(checkbox).val();
      if ($(checkbox).is(':checked')) {
        $('.field-its-edit-text', context).each(function () {
          Drupal.its.filter.show(meta_name, this);
        });
        Drupal.its.filter.activeCategories.push(meta_name);
      }
      else {
        $('.field-its-edit-text', context).each(function () {
          Drupal.its.filter.hide(meta_name, this);
        });
        if (Drupal.its.filter.activeCategories.indexOf(meta_name) != -1) {
          delete Drupal.its.filter.activeCategories[Drupal.its.filter.activeCategories.indexOf(meta_name)];
        }
      }
    },

    setGlobalTranslateRule: function(index, fieldset) {
      var self = Drupal.behaviors.its_text;
      var $fieldset    = fieldset.jquery ? fieldset : $(fieldset),
        $selectorInput = $fieldset.find('#edit-its-translate-de-' + index + '-its-selector'),
        $valueSelect   = $fieldset.find('#edit-its-translate-de-' + index + '-its-value');

      if (!$selectorInput.hasClass('its-global-processed')) {
        $selectorInput.addClass('its-global-processed');
        $selectorInput.change(function() {
          self.setGlobalTranslateRule(index, $fieldset);
        });
      }

      if (!$valueSelect.hasClass('its-global-processed')) {
        $valueSelect.addClass('its-global-processed');
        $valueSelect.change(function() {
          self.setGlobalTranslateRule(index, $fieldset);
        });
      }

      if ($selectorInput.length > 0 && $valueSelect.length > 0 && $selectorInput.val() != '' && $valueSelect.val() != '') {
        var addRuleObject = {
          selector: $selectorInput.val(),
          translate: ($valueSelect.val() === 'yes') ? true : false,
          type: self.translateRuleController.NAME
        };

        var checkbox = $('.meta-data input.form-checkbox[value=its_translate]', self.context),
          selector = null;
        if (checkbox.is(':checked')) {
          var data = checkbox.data();
          if (data && data.selector) {
            selector = data.selector;
          }
          $('.field-its-edit-text', self.context).each(function () {
            Drupal.its.filter.hide('its_translate', this, selector);
          });
        }

        if (typeof self.translateRuleKeyMap[index] === 'undefined') {
          self.translateRuleKeyMap[index] = self.translateRuleController.rules.length;
          self.translateRuleController.addSelector(addRuleObject);
          console.log('added', addRuleObject);
        }
        else {
          self.translateRuleController.rules[self.translateRuleKeyMap[index]] = addRuleObject;
          console.log('updated', addRuleObject);
        }

        if (checkbox.is(':checked')) {
          $('.field-its-edit-text', self.context).each(function () {
            Drupal.its.filter.show('its_translate', this, selector);
          });
        }
      }
    }
  };

})(jQuery);
