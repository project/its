SUMMARY
=======

The purpose of this Module is to integrate the Internationalization Tag Set
(ITS2.0) into Drupal. ITS 2.0 is designed to foster the creation of
multilingual Web content, focusing on HTML, XML based formats in general,
and to leverage localization workflows.

The following data categories from the ITS2.0 Standard are currently supported:

* Translate [http://www.w3.org/TR/its20/#trans-datacat]
* Localization Note [http://www.w3.org/TR/its20/#locNote-datacat]
* Domain [http://www.w3.org/TR/its20/#domain]
* Provenance [http://www.w3.org/TR/its20/#provenance]
* Allowed Characters [http://www.w3.org/TR/its20/#allowedchars]
* Storage Size [http://www.w3.org/TR/its20/#storagesize]

It creates fields for node types with multilingual support enabled. For this
global values there are also site wide defaults and content type defaults.
The values of this fields will be added to a linked rules file, when a node
is being viewed. To have a valid page markup, you have to use a HTML5 theme!

Also there is a integration with the WYSIWYG Module, which allows you to add
the data categories locally to the content, with help of the WYSIWYG Editor.

Currently in development is a new Language Management view, where the user has
the possibility to view all the values of the data categories and edit this,
without having access to edit the content. It uses the ITS2.0 JQuery selector
plugin.

There is a TMGMT translator available which uses the ITS2.0 Standard,
see TMGMT Translator Linguaserve [http://drupal.org/sandbox/kfritsche/1908422].
However main functionality to use TMGMT and ITS is in this module and can
be used by other TMGMT translator modules.

This module provides hooks to alter data categories and give you the
possibility to add additional data categories, See the its.api.php.

REQUIREMENTS
============

Internationalization (i18n)
  http://drupal.org/project/i18n

Entity API (entity)
  http://drupal.org/project/entity

Variable (variable)
  http://drupal.org/project/variable

Libraries (libraries)
  http://drupal.org/project/libraries

  Download provided by the libraries module for:
    jQuery ITS Parser - http://plugins.jquery.com/its-parser/
    jQuery Simpletip - http://craigsworks.com/projects/simpletip/


OPTIONAL (but recommended)
--------------------------

Translation Management Tool (tmgmt)
  http://drupal.org/project/tmgmt

INSTALLATION
============

* Install the required modules (Libraries,Variable,Translation Management Tool,
  Internationalization, Entity API) as usual (http://drupal.org/node/70151).

* Download the Simpletip jQuery Plugin from
  http://craigsworks.com/projects/simpletip/

* Copy the downloaded file to sites/all/libraries/jquery.simpletip .
  File should be named jquery.simpletip.js

* Download the ITS Parser jQuery Plugin from
  http://plugins.jquery.com/its-parser/ (At least Version 2.0)

* Extract the downloaded archive into sites/all/libraries/jquery.its-parser .
  There should be  at least a file named jquery.its-parser.js in this directory.

* Install this module as usual, see http://drupal.org/node/70151
  for further information.

* Enable ITS Support for a node type at the node type settings form.
  e.g. http://your-domain.com/admin/structure/types/manage/article
    under "International Tag Set settings"
    I18N support for this bundle has to be enabled.


CONFIGURATION
=============

* Configure user permissions in Administration -> People -> Permissions:

  - Administer International Tag Set (administer its)

    A user who should be allowed to change global site settings for this
    module, should get this permission.

  - Edit ITS data category fields (edit its fields)

    Users in roles with this permission can change the value of the
    automatically created ITS fields.

  - Allow access to the language management page for each node
      (edit language management)

    Users in roles with this permission are allowed to access the language
    management tab and view the content of the ITS fields. If they also have
    edit permissions they can edit and add new meta data here.

* Enable ITS Support or set default values for a node type in Structure ->
  Content types -> edit a content type

  - under Publishing options set Multilingual support to
    "Enabled, with translation"

  - under International Tag Set settings you can set
    * default values for the data categories
    * choose a taxonomy field, which should be used for the domain category
      choose "Use extra domain field", if this should be a free text field

* Under Configuration -> Regional and language -> International Tag Set
  Integration you can set site wide default values for the data categories.

CUSTOMIZATION
=============

* This module provides hooks to alter data categories and give you the
  possibility to add additional data categories, See the its.api.php.

CONTACT
=======

Current maintainer:
* Karl Fritsche (kfritsche) - http://drupal.org/user/619702

This project has been sponsored by:
* Cocomore AG
  http://www.cocomore.com

* MultilingualWeb-LT Working Group
  http://www.w3.org/International/multilingualweb/lt/

* European Commission - CORDIS - Seventh Framework Programme (FP7)
  http://cordis.europa.eu/fp7/dc/index.cfm
