<?php

/**
 * @file
 * Theme functions of this module.
 */

/**
 * Add themed meta data to HTML Head.
 *
 * If viewing multiple nodes or node is only previewed,
 * saving attributes to the global $its_node_attributes.
 * This global should be added to your theme at the content div.
 *
 * @param array $variables
 *   type: name of the meta data
 *   view_mode: full, teaser, export
 *   meta_data: value of the meta data
 */
function theme_its_meta_data($variables) {
  $name = $variables['type'];
  $output_name = str_replace('_', '-', $name);
  if (!empty($variables['meta_data'])) {
    if ($variables['view_mode'] == 'full') {
      $element = array(
        '#tag' => 'meta',
        '#attributes' => array(
          'name' => 'its-' . $output_name,
          'content' => $variables['meta_data'],
        ),
      );
      drupal_add_html_head($element, $name);
    }
    else {
      if (empty($GLOBALS['its_node_attributes'])) {
        $GLOBALS['its_node_attributes'] = array();
      }
      $GLOBALS['its_node_attributes']['its-' . $output_name] = $variables['meta_data'];
    }
  }
}

/**
 * Theme of data category domain.
 *
 * Creates a meta tag with the selected domains.
 * On view_mode export returns this rendered meta tag.
 * On view_mode full adds this meta tag to HTML head and returns the rule.
 *
 * @param array $variables
 *   type: name of the meta data
 *   view_mode: full, teaser, export
 *   meta_data: value of the meta data
 *
 * @return string
 *   On view_mode export returns the rendered meta tag.
 *   On view_mode full adds this meta tag to HTML head and returns the rule.
 */
function theme_its_meta_data__domain($variables) {
  if (!empty($variables['meta_data'])) {
    $element = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => array(
        'name' => 'dcterms:subject',
        'content' => $variables['meta_data'],
      ),
    );
    if ($variables['view_mode'] == 'full') {
      drupal_add_html_head($element, $variables['type']);
      return theme('its_meta_data_rule', array(
        'rule_name' => 'domainRule',
        'attributes' => array(
          'selector' => '//body',
          'domainPointer' => '/html/head/meta[@name=\'dcterms:subject\']/@content',
        ),
      ));
    }
    elseif ($variables['view_mode'] == 'export') {
      return drupal_render($element);
    }
  }
}

/**
 * Theme of data category localization note.
 *
 * @param array $variables
 *   type: name of the meta data
 *   view_mode: full, teaser, export
 *   meta_data: value of the meta data
 */
function theme_its_meta_data__loc_note($variables) {
  // TODO globals...
  if (!empty($variables['meta_data'])) {
    if (empty($GLOBALS['its_node_attributes'])) {
      $GLOBALS['its_node_attributes'] = array();
    }
    if (!empty($variables['meta_data']['note'])) {
      $GLOBALS['its_node_attributes']['its-loc-note'] = $variables['meta_data']['note'];
      if (!empty($variables['meta_data']['type'])) {
        $GLOBALS['its_node_attributes']['its-loc-note-type'] = $variables['meta_data']['type'];
      }
    }
  }
}

/**
 * Theme of data category translate.
 *
 * Creates a rule with the given selector and value.
 * Example output:
 *  <its:translateRule translate="no" selector="//code"/>
 *
 * @param array $variables
 *   type: name of the meta data
 *   view_mode: full, teaser, export
 *   meta_data: value of the meta data, array with value and selector
 *
 * @return null|string
 *   themed rule or null if no valid data was given
 */
function theme_its_meta_data__translate($variables) {
  if (empty($variables['meta_data']['value']) || empty($variables['meta_data']['selector'])) {
    return NULL;
  }
  return theme('its_meta_data_rule', array(
    'rule_name' => 'translateRule',
    'attributes' => array(
      'translate' => $variables['meta_data']['value'],
      'selector' => $variables['meta_data']['selector'],
    ),
  ));
}

/**
 * Theme of data category provenance (revision agent).
 *
 * Creates a rule with the given selector and value.
 * Example output:
 *  <its:transProvRule selector="//item" transRevOrg="Linguaserve" transRevPerson="12345"/>
 *
 * @param array $variables
 *   type: name of the meta data
 *   view_mode: full, teaser, export
 *   meta_data: value of the meta data, array with value and selector
 *
 * @return null|string
 *   themed rule or null if no valid data was given
 */
function theme_its_meta_data__revision_agent($variables) {
  if (!empty($variables['meta_data']) && !empty($variables['meta_data']['its'])) {
    $attr = array('selector' => '//body');
    $meta = explode(';', $variables['meta_data']['its']);
    $attr['transRevOrg'] = $meta[0];
    if (!empty($meta[1])) {
      $attr['transRevPerson'] = $meta[1];
    }
    return theme('its_meta_data_rule', array('rule_name' => 'transProvRule', 'attributes' => $attr));
  }
}

/**
 * Theme of data category provenance (translation agent).
 *
 * Creates a rule with the given selector and value.
 * Example output:
 *  <its:transProvRule selector="//item" transOrg="Linguaserve" transPerson="12345"/>
 *
 * @param array $variables
 *   type: name of the meta data
 *   view_mode: full, teaser, export
 *   meta_data: value of the meta data, array with value and selector
 *
 * @return null|string
 *   themed rule or null if no valid data was given
 */
function theme_its_meta_data__translation_agent($variables) {
  if (!empty($variables['meta_data']) && !empty($variables['meta_data']['its'])) {
    $attr = array('selector' => '//body');
    $meta = explode(';', $variables['meta_data']['its']);
    $attr['transOrg'] = $meta[0];
    if (!empty($meta[1])) {
      $attr['transPerson'] = $meta[1];
    }
    return theme('its_meta_data_rule', array('rule_name' => 'transProvRule', 'attributes' => $attr));
  }
}

/**
 * Theme of a general rule.
 *
 * Used by all other specific theme rules.
 *
 * @param array $variables
 *   rule_name: name of the tag
 *   attributes: array of attributes
 *   content: content of the tag (optional)
 *
 * @return string
 *   themed rule
 */
function theme_its_meta_data_rule($variables) {
  $namespace = isset($variables['namespace']) ? $variables['namespace'] : 'its';
  $html = '<' . $namespace . ':' . $variables['rule_name'];
  $html .= drupal_attributes($variables['attributes']);
  if (empty($variables['content'])) {
    $html .= ' />';
  }
  else {
    $html .= '>' . $variables['content'] . '</' . $namespace . ':' . $variables['rule_name'] . '>';
  }
  return $html;
}

/**
 * Creates the its:rules tag, if there are rules.
 *
 * @param array $variables
 *   rules: themed rules
 *
 * @return string
 *   themed its:rules tag
 */
function theme_its_meta_data_rules($variables) {
  if (!empty($variables['rules'])) {
    foreach ($variables['rules'] as $key => $value) {
      if (empty($value)) {
        unset($variables['rules'][$key]);
      }
      if (isset($variables['html'])) {
        $variables['rules'][$key] = preg_replace('/\/(\w)/', '/' . $variables['html'] . ':$1', $value);
      }
    }

    $element = array(
      '#tag' => 'its:rules',
      '#attributes' => array('xmlns:its' => "http://www.w3.org/2005/11/its", 'version' => '2.0'),
      '#value' => "\t" . implode("\n\t", $variables['rules']) . "\n",
    );
    return theme('html_tag', array('element' => $element));
  }
}
