<?php

/**
 * @file
 * WYSIWYG plugin file for ITS module.
 * Defines a new Button for WYSIWYG Editors,
 * which allows to add ITS attributes.
 */

/**
 * Define a Wysiwyg plugin, which allows to add ITS attributes.
 *
 * @return array
 *   Meta information about the buttons provided by this plugin.
 */
function its_its_plugin() {

  $plugins['its_translate'] = array(
    // The plugin's title; defaulting to its internal name ('its').
    'title' => t('International Tag Set Translate'),
    // The (vendor) homepage of this plugin; defaults to ''.
    'vendor url' => 'http://drupal.org/project/its',
    // The button image filename; defaults to '[plugin-name].png'.
    'icon file' => 'translate.png',
    // The button title to display on hover.
    'icon title' => t('Do not translate marked text. You decide whether the marked text passage can be translated or not.'),
    // An array of settings for this button. Required, but API is still in flux.
    'settings' => array(
      'validAttributes' => array('translate'),
    ),
    'buttons' => array('its_translate' => t('Translate')),
    'load' => TRUE,
    'internal' => FALSE,
    // The path to the button's icon; defaults to
    // '/[path-to-module]/[plugins-directory]/[plugin-name]/images'.
    'icon path' => drupal_get_path('module', 'its') . '/plugins/images',
    // An alternative path to the integration JavaScript; defaults to
    // '[path-to-module]/[plugins-directory]/[plugin-name]'.
    'js path' => drupal_get_path('module', 'its') . '/plugins',
    // An alternative filename of the integration JavaScript; defaults to
    // '[plugin-name].js'.
    'js file' => 'its.wysiwyg.js',
    // An alternative path to the integration stylesheet; defaults to
    // '[path-to-module]/[plugins-directory]/[plugin-name]'.
    'css path' => drupal_get_path('module', 'its'),
    // An alternative filename of the integration stylesheet; defaults to
    // '[plugin-name].css'.
    'css file' => 'its.lm.css',
    'key map' => 't',
    'lm title' => t('Translate - yes/no?'),
    'lm description' => t('You decide whether the marked text passage can be translated or not.'),
  );

  $plugins['its_locale'] = array(
    // The plugin's title; defaulting to its internal name ('its').
    'title' => t('International Tag Set Locale filter'),
    // The (vendor) homepage of this plugin; defaults to ''.
    'vendor url' => 'http://drupal.org/project/its',
    // The button image filename; defaults to '[plugin-name].png'.
    'icon file' => 'locale.png',
    // The button title to display on hover.
    'icon title' => t('Only translate for the following locales. (locale filter)'),
    // An array of settings for this button. Required, but API is still in flux.
    'settings' => array(
      'validAttributes' => array('its-locale-filter-list'),
    ),
    'buttons' => array('its_locale' => t('Locale filter')),
    'load' => TRUE,
    'internal' => FALSE,
    // The path to the button's icon; defaults to
    // '/[path-to-module]/[plugins-directory]/[plugin-name]/images'.
    'icon path' => drupal_get_path('module', 'its') . '/plugins/images',
    // An alternative path to the integration JavaScript; defaults to
    // '[path-to-module]/[plugins-directory]/[plugin-name]'.
    'js path' => drupal_get_path('module', 'its') . '/plugins',
    // An alternative filename of the integration JavaScript; defaults to
    // '[plugin-name].js'.
    'js file' => 'its.wysiwyg.js',
    // An alternative path to the integration stylesheet; defaults to
    // '[path-to-module]/[plugins-directory]/[plugin-name]'.
    'css path' => drupal_get_path('module', 'its'),
    // An alternative filename of the integration stylesheet; defaults to
    // '[plugin-name].css'.
    'css file' => 'its.lm.css',
    'key map' => 'f',
    'lm title' => t('Market-specific language'),
    'lm description' => t('I.e. There can be different rules for the product description in the canadian and british market.'),
  );

  $plugins['its_textAnalysis'] = array(
    // The plugin's title; defaulting to its internal name ('its').
    'title' => t('International Tag Set Text analysis'),
    // The (vendor) homepage of this plugin; defaults to ''.
    'vendor url' => 'http://drupal.org/project/its',
    // The button image filename; defaults to '[plugin-name].png'.
    'icon file' => 'entity.png',
    // The button title to display on hover.
    'icon title' => t('Specify Concepts that may require special handling for translation. (Text analysis)'),
    // An array of settings for this button. Required, but API is still in flux.
    'settings' => array(
      'validAttributes' => array(
        'its-ta-confidence',
        'its-ta-class-ref',
        'its-ta-source-ref',
        'its-ta-ident',
        'its-ta-ident-ref',
        'its-annotators-ref',
      ),
    ),
    'buttons' => array('its_textAnalysis' => t('Text analysis')),
    'load' => TRUE,
    'internal' => FALSE,
    // The path to the button's icon; defaults to
    // '/[path-to-module]/[plugins-directory]/[plugin-name]/images'.
    'icon path' => drupal_get_path('module', 'its') . '/plugins/images',
    // An alternative path to the integration JavaScript; defaults to
    // '[path-to-module]/[plugins-directory]/[plugin-name]'.
    'js path' => drupal_get_path('module', 'its') . '/plugins',
    // An alternative filename of the integration JavaScript; defaults to
    // '[plugin-name].js'.
    'js file' => 'its.wysiwyg.js',
    // An alternative path to the integration stylesheet; defaults to
    // '[path-to-module]/[plugins-directory]/[plugin-name]'.
    'css path' => drupal_get_path('module', 'its'),
    // An alternative filename of the integration stylesheet; defaults to
    // '[plugin-name].css'.
    'css file' => 'its.lm.css',
    'key map' => 'a',
    'lm description' => t('A word is to be a homonym, means more meanings for one word. You can fill in what the meaning of this specific word is.'),
  );

  $plugins['its_locNote'] = array(
    // The plugin's title; defaulting to its internal name ('its').
    'title' => t('International Tag Set Localization note'),
    // The (vendor) homepage of this plugin; defaults to ''.
    'vendor url' => 'http://drupal.org/project/its',
    // The button image filename; defaults to '[plugin-name].png'.
    'icon file' => 'locNote.png',
    // The button title to display on hover.
    'icon title' => t('Add a localization note for the translator. (Localization note)'),
    // An array of settings for this button. Required, but API is still in flux.
    'settings' => array(
      'validAttributes' => array('its-loc-note', 'its-loc-note-type'),
    ),
    'buttons' => array('its_locNote' => t('Localization note')),
    'load' => TRUE,
    'internal' => FALSE,
    // The path to the button's icon; defaults to
    // '/[path-to-module]/[plugins-directory]/[plugin-name]/images'.
    'icon path' => drupal_get_path('module', 'its') . '/plugins/images',
    // An alternative path to the integration JavaScript; defaults to
    // '[path-to-module]/[plugins-directory]/[plugin-name]'.
    'js path' => drupal_get_path('module', 'its') . '/plugins',
    // An alternative filename of the integration JavaScript; defaults to
    // '[plugin-name].js'.
    'js file' => 'its.wysiwyg.js',
    // An alternative path to the integration stylesheet; defaults to
    // '[path-to-module]/[plugins-directory]/[plugin-name]'.
    'css path' => drupal_get_path('module', 'its'),
    // An alternative filename of the integration stylesheet; defaults to
    // '[plugin-name].css'.
    'css file' => 'its.lm.css',
    'key map' => 'n',
    'lm title' => t('Further annotations'),
    'lm description' => t('You can brief the translation with annotations about a certain text passage, like Please check this quote by consulting Mr. Mueller.'),
  );

  $tmp = language_list();
  $language_list = array();
  foreach ($tmp as $lang) {
    $language_list[$lang->language] = $lang->name;
  }

  $plugins['its_lang'] = array(
    // The plugin's title; defaulting to its internal name ('its').
    'title' => t('International Tag Set Language'),
    // The (vendor) homepage of this plugin; defaults to ''.
    'vendor url' => 'http://drupal.org/project/its',
    // The button image filename; defaults to '[plugin-name].png'.
    'icon file' => 'lang.png',
    // The button title to display on hover.
    'icon title' => t('Set the language of the marked content. (lang)'),
    // An array of settings for this button. Required, but API is still in flux.
    'settings' => array(
      'validAttributes' => array('lang'),
      'enabledLanguages' => $language_list,
    ),
    'buttons' => array('its_lang' => t('Language')),
    'load' => TRUE,
    'internal' => FALSE,
    // The path to the button's icon; defaults to
    // '/[path-to-module]/[plugins-directory]/[plugin-name]/images'.
    'icon path' => drupal_get_path('module', 'its') . '/plugins/images',
    // An alternative path to the integration JavaScript; defaults to
    // '[path-to-module]/[plugins-directory]/[plugin-name]'.
    'js path' => drupal_get_path('module', 'its') . '/plugins',
    // An alternative filename of the integration JavaScript; defaults to
    // '[plugin-name].js'.
    'js file' => 'its.wysiwyg.js',
    // An alternative path to the integration stylesheet; defaults to
    // '[path-to-module]/[plugins-directory]/[plugin-name]'.
    'css path' => drupal_get_path('module', 'its'),
    // An alternative filename of the integration stylesheet; defaults to
    // '[plugin-name].css'.
    'css file' => 'its.lm.css',
    'key map' => 'l',
    'lm description' => t('Is needed for automatic translation.'),
  );

  $plugins['its_dir'] = array(
    // The plugin's title; defaulting to its internal name ('its').
    'title' => t('International Tag Set Directionality'),
    // The (vendor) homepage of this plugin; defaults to ''.
    'vendor url' => 'http://drupal.org/project/its',
    // The button image filename; defaults to '[plugin-name].png'.
    'icon file' => 'mlw_lt.png',
    // The button title to display on hover.
    'icon title' => t('Set the read direction of the marked content. (dir)'),
    // An array of settings for this button. Required, but API is still in flux.
    'settings' => array(
      'validAttributes' => array('dir'),
    ),
    'buttons' => array('its_dir' => t('Directionality')),
    'load' => TRUE,
    'internal' => FALSE,
    'icon path' => drupal_get_path('module', 'its') . '/plugins/images',
    'js path' => drupal_get_path('module', 'its') . '/plugins',
    'js file' => 'its.wysiwyg.js',
    'css path' => drupal_get_path('module', 'its'),
    'css file' => 'its.lm.css',
    'key map' => 'd',
    'lm title' => t('Read direction'),
    'lm description' => t('I.e. Arabic is to be read from right to left.'),
  );

  $plugins['its_terminology'] = array(
    // The plugin's title; defaulting to its internal name ('its').
    'title' => t('International Tag Set Terminology'),
    // The (vendor) homepage of this plugin; defaults to ''.
    'vendor url' => 'http://drupal.org/project/its',
    // The button image filename; defaults to '[plugin-name].png'.
    'icon file' => 'mlw_lt.png',
    // The button title to display on hover.
    'icon title' => t('Mark terms and optionally associate them with information. (Terminology)'),
    // An array of settings for this button. Required, but API is still in flux.
    'settings' => array(
      'validAttributes' => array('term','its-term-info-ref', 'its-term-confidence'),
    ),
    'buttons' => array('its_terminology' => t('Terminology  ')),
    'load' => TRUE,
    'internal' => FALSE,
    'icon path' => drupal_get_path('module', 'its') . '/plugins/images',
    'js path' => drupal_get_path('module', 'its') . '/plugins',
    'js file' => 'its.wysiwyg.js',
    'css path' => drupal_get_path('module', 'its'),
    'css file' => 'its.lm.css',
    'key map' => 's',
    'lm description' => t('You can use your terminology database. i.e. which kind of screw is?'),
  );

  drupal_add_library('system', 'ui.dialog');
  if (($library = libraries_load('jquery.its-parser')) && empty($library['loaded'])) {
    drupal_set_message(t("jQuery ITS-Parser library couldn't be found. ITS Plugins for WYSIWYG editor will not work."));
  }
  else {
    drupal_add_js(drupal_get_path('module', 'its') . '/its.lm.js', 'file');
  }

  return $plugins;
}
