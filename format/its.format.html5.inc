<?php

/**
 * @file
 * A TMGMT file format class file.
 * Defines a formatter that exports a job as a HTML5 file.
 */

/**
 * Export to HTML5+ITS2 format.
 */
class TMGMTFileformatHtml5Its2 implements TMGMTFileFormatInterface {

  /**
   * Contains a reference to the currently being exported job.
   *
   * @var TMGMTJob
   */
  protected $job;

  /**
   * Current meta data for the current TMGMTJobItem.
   *
   * @var array
   */
  protected $metaData;

  /**
   * Indent for the output.
   *
   * @var int
   */
  protected $indent = 4;

  /**
   * Stores already used keys to allow generating unique keys.
   *
   * @var array
   */
  protected $uniqueKeys = array();

  /**
   * Convert a JobItem to a HTML5 representation.
   *
   * @param TMGMTJobItem $item
   *   JobItem to add to the file
   *
   * @return array
   *   A array with #text and #its keys.
   *   The #text has the HTML content for this JobItem.
   *   The #key  is a array with all ITS meta data from this JobItem.
   */
  protected function addItem(TMGMTJobItem $item) {
    $return = array(
      '#text' => '',
      '#its' => array(
        '#meta' => array(),
        '#rules' => array(),
        '#addtional_attributes' => array(),
      ),
    );

    $meta_data = its_tmgmt_item_get_meta_data($item);
    $this->metaData = $meta_data;

    foreach ($meta_data as $type => $value) {
      switch ($type) {
        case 'domain':
          if (!empty($value[0]['its'])) {
            if (count($this->job->getItems()) == 1) {
              $return['#its']['#meta'][] = '<meta name="dcterms.subject" content="' . $value[0]['its'] . '">';
              $return['#its']['#rules'][] = '<its:domainRule selector="//body" domainPointer="/html/head/meta[\'dcterms.subject\']/@content"/>';
            }
            else {
              $GLOBALS['its_node_attributes']['domain'] = $value[0]['its'];
              $return['#its']['#rules'][] = '<its:domainRule selector="//body" domainPointer="//div[@id=\'asset-' . $item->tjiid . '\']/@domain"/>';
            }
          }
          break;

        case 'translate':
          foreach ($value as $val) {
            $val = theme('its_meta_data__translate', array('meta_data' => $val));
            if (!empty($val)) {
              $return['#its']['#rules'][] = $val;
            }
          }
          break;

        case 'loc_note':
          theme('its_meta_data__loc_note', array('meta_data' => $value[0]));
          break;

        case 'revision_agent':
        case 'translation_agent':
          if (!empty($value[0])) {
            $val = theme('its_meta_data__' . $type, array('meta_data' => $value[0]));
          }
          if (!empty($val)) {
            $return['#its']['#rules'][] = $val;
          }
          break;
      }
    }
    $return['#its']['#addtional_attributes'] = $GLOBALS['its_node_attributes'];
    unset($GLOBALS['its_node_attributes']);

    $return['#text'] = $this->addField($item->getData());

    return $return;
  }

  /**
   * Create div structure from TMGMTJobItem data.
   *
   * @param array $data
   *   TMGMTJobItem data array
   * @param array $keys
   *   (internal use only - recursive)
   * @param null|string $field_name
   *   (internal use only - recursive)
   *
   * @return string
   *   HTML presentation of the TMGMTJobItem.
   */
  protected function addField($data, $keys = array(), $field_name = NULL) {
    if (isset($data['#text'])) {
      return str_repeat(" ", $this->indent) . $data['#text'] . "\n";
    }
    else {
      $text = '';
      foreach (element_children($data) as $key) {
        if (isset($this->metaData['allowed_characters'][$key]) || isset($this->metaData['storage_size'][$key])) {
          $field_name = $key;
        }
        $child_keys = $keys;
        $child_keys[] = $key;
        $id = $this->getID($child_keys);
        $text .= str_repeat(" ", $this->indent) . '<div id="' . $id . '"';
        if ((isset($data['#translate']) && $data['#translate'] == FALSE) || (isset($data[$key]['#translate']) && $data[$key]['#translate'] == FALSE)) {
          $text .= ' translate="no"';
        }
        // Next node is text, add allowed characters and storage size.
        if (!empty($field_name) && isset($data[$key]['#text'])) {
          if (isset($this->metaData['allowed_characters'][$field_name])) {
            $text .= ' its-allowed-characters="' . check_plain($this->metaData['allowed_characters'][$field_name]) . '"';
          }
          if (isset($this->metaData['storage_size'][$field_name])) {
            $text .= ' its-storage-size="' . $this->metaData['storage_size'][$field_name] . '"';
          }
        }
        $this->indent += 2;
        $text .= ">\n" . $this->addField($data[$key], $child_keys, $field_name);
        $this->indent -= 2;
        $text .= str_repeat(" ", $this->indent) . '</div> <!-- end div ' . $id . ' -->' . "\n";
        $field_name = NULL;
      }
      return $text;
    }
  }

  /**
   * Get unique ID for given keys.
   *
   * @param array $keys
   *   Array of keys.
   *
   * @return string
   *   A unique key.
   */
  protected function getID($keys) {
    $key = $this->job->tjid . '-' . implode('-', $keys);

    if (isset($this->uniqueKeys[$key])) {
      $unique_key = $key . '-' . $this->uniqueKeys[$key];
      $this->uniqueKeys[$key]++;
    }
    else {
      $this->uniqueKeys[$key] = 1;
      $unique_key = $key;
    }
    return $unique_key;
  }

  /**
   * Implements TMGMTFileExportInterface::export().
   */
  public function export(TMGMTJob $job) {
    $items = array();
    $its_meta = array();
    $its_rules = array();
    $this->job = $job;
    foreach ($job->getItems() as $item) {
      $items[$item->tjiid] = $this->addItem($item);
      $its_meta += $items[$item->tjiid]['#its']['#meta'];
      foreach ($items[$item->tjiid]['#its']['#rules'] as $rule) {
        $its_rules[] = preg_replace('/selector="(\/\/body([^"]*)|([^"]*))"/', 'selector="//div[@id=\'asset-' . $item->tjiid . '\']\2\3"', $rule);
      }
    }

    $uri = NULL;
    if (!empty($its_rules)) {
      $directory = file_build_uri('its_html5_rules');
      $uri = $directory . "/" . $job->tjid;
      file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
      $xml = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
      $its_rules[] = theme('its_meta_data_rule', array(
        'rule_name' => 'withinTextRule',
        'attributes' => array(
          'withinText' => 'yes',
          'selector' => '//b|//em|//i|//strong|//span',
        ),
      ));
      $xml .= theme('its_meta_data_rules', array('rules' => $its_rules));
      file_save_data($xml, $uri, FILE_EXISTS_REPLACE);
    }

    return theme('its_html5_template', array(
      'tjid' => $job->tjid,
      'source_language' => $job->source_language,
      'target_language' => $job->target_language,
      'items' => $items,
      'its_meta' => $its_meta,
      'its_rules' => file_create_url($uri),
    ));
  }

  /**
   * Convert a HTML node to a TMGMTJobItem data array.
   *
   * @param SimpleXMLElement $item
   *   Element which should be extracted.
   *
   * @return array
   *   TMGMTJobItem data array.
   */
  protected function extractItem($item) {
    $data = array();
    foreach ($item->children() as $child) {
      $key = explode('-', (string) $child['id']);
      $key = array_pop($key);
      $child_childs = $child->children();
      if (count($child_childs) == 0 || !isset($child_childs[0]['id']) || strpos($child_childs[0]['id'], (string) $child['id']) === FALSE) {
        if (count($child_childs) != 0) {
          $data[$key]['#text'] = $child->children()->asXML();
        }
        else {
          $data[$key]['#text'] = (string) $child;
        }
      }
      else {
        $data[$key] = $this->extractItem($child);
      }
    }
    return $data;
  }

  /**
   * Normalize a selector.
   *
   * Replace //div[@id='asset-ID'] to //body.
   *
   * @param SimpleXMLElement $selector
   *   Selector element
   *
   * @return string
   *   Normalized selector
   */
  protected  function normalizeSelector($selector) {
    return preg_replace('/\/\/div\[@id=\'asset-\d+\'\]/', '//body', (string) $selector);
  }

  /**
   * Implements TMGMTFileExportInterface::import().
   */
  public function import($imported_file) {
    $dom = new DOMDocument();
    @$dom->loadHTMLFile($imported_file);
    $xml = simplexml_import_dom($dom);

    $xml->registerXPathNamespace('its', 'http://www.w3.org/2005/11/its');

    $global_meta_data = array();
    // Get global meta data.
    foreach ($xml->xpath("//head/meta") as $meta) {
      if (isset($meta['name']) && isset($meta['content'])) {
        $global_meta_data[$meta['name']] = $meta['content'];
      }
    }

    $rules = array();
    foreach ($xml->xpath("//head/rules/*") as $rule) {
      switch ($rule->getName()) {
        case 'translaterule':
          if ($rule['selector'] == '/html/head/title') {
            continue;
          }
          if (preg_match('/\'asset-(\d+)\'/', $rule['selector'], $matches)) {
            $tjiid = $matches[1];
            if (!isset($rules[$tjiid])) {
              $rules[$tjiid] = array();
            }
            if (!isset($rules[$tjiid]['translate'])) {
              $rules[$tjiid]['translate'] = array();
            }

            $rules[$tjiid]['translate'][] = array(
              'selector' => $this->normalizeSelector($rule['selector']),
              'value' => (string) $rule['translate'],
            );
          }
          else {
            $global_meta_data['translate'][] = array(
              'selector' => (string) $rule['selector'],
              'value' => (string) $rule['translate'],
            );
          }
          break;

        case 'domainrule':
          if (preg_match('/\'asset-(\d+)\'/', $rule['selector'], $matches)) {
            $tjiid = $matches[1];
            $val = $xml->xpath($rule['domainpointer']);
            if (isset($val[0])) {
              if (!isset($rules[$tjiid])) {
                $rules[$tjiid] = array();
              }
              $rules[$tjiid]['domain'] = (string) $val[0]['domain'];
            }
          }
          break;
      }
    }

    $data = array();
    foreach ($xml->xpath("//body/div") as $job_item) {
      $tjiid = substr((string) $job_item['id'], 6);

      $this->metaData = $global_meta_data;
      if (isset($rules[$tjiid])) {
        $this->metaData += $rules[$tjiid];
      }

      foreach ($job_item->attributes() as $attribute) {
        switch ($attribute->getName()) {
          case 'domain':
            if (!isset($this->metaData['domain'])) {
              $this->metaData['domain'] = (string) $attribute;
            }
            break;

          case 'its-loc-note':
            if (!isset($this->metaData['loc_note'])) {
              $this->metaData['loc_note'] = array(array());
            }
            $this->metaData['loc_note'][0]['note'] = (string) $attribute;
            break;

          case 'its-loc-note-type':
            if (!isset($this->metaData['loc_note'])) {
              $this->metaData['loc_note'] = array();
            }
            $this->metaData['loc_note'][0]['type'] = (string) $attribute;
            break;
        }
      }

      $data[$tjiid] = $this->extractItem($job_item);

      $data[$tjiid]['#meta_data'] = $this->metaData;
    }
    return $data;
  }

  /**
   * Implements TMGMTFileExportInterface::validateImport().
   */
  public function validateImport($imported_file) {
    $dom = new DOMDocument();
    if (!@$dom->loadHTMLFile($imported_file)) {
      return FALSE;
    }
    $xml = simplexml_import_dom($dom);

    $job_id = $xml->xpath('//head/title');
    $job_id = (string) $job_id[0];
    $job_id = explode(':', $job_id);
    if (empty($job_id[1])) {
      return FALSE;
    }
    $job_id = $job_id[1];

    // Attempt to load job.
    if (!$job = tmgmt_job_load($job_id)) {
      return FALSE;
    }

    $target_language = $xml->xpath('/html/@lang');
    $target_language = $target_language[0];
    if (empty($target_language['lang'])) {
      return FALSE;
    }
    $target_language = (string) $target_language['lang'];

    // Check language.
    if ($target_language != $job->target_language) {
      return FALSE;
    }

    // Validation successful.
    return $job;
  }

}
