<?php
/**
 * @file
 * ITS Administration forms file.
 */


/**
 * Admin Menu callback.
 *
 * @return string
 *   HTML output of the admin form.
 */
function its_admin_configuration() {
  return drupal_get_form('its_admin_form');
}

/**
 * Site wide default settings form for ITS data categories.
 *
 * @return array
 *   system_settings_form expanded with its options.
 */
function its_admin_form() {

  $form = array();

  $form['its_enabled'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enable ITS for a type'),
    '#description' => t('Declare which node types should have support for ITS.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );

  $enabled = variable_get('its_enabled', array());
  foreach (entity_get_info() as $entity_type => $info) {
    // Only fieldable entities can have ITS support.
    if (!$info['fieldable']) {
      continue;
    }
    $options = array();
    foreach ($info['bundles'] as $bundle_key => $bundle_info) {
      $options[$bundle_key] = $bundle_info['label'];
      if ($entity_type == 'node') {
        if (!locale_multilingual_node_type($bundle_key)) {
          unset($options[$bundle_key]);
        }
      }
    }
    $form['its_enabled'][$entity_type] = array(
      '#type' => 'checkboxes',
      '#title' => $info['label'],
      '#options' => $options,
      '#default_value' => isset($enabled[$entity_type]) ? $enabled[$entity_type] : array(),
    );
    if ($entity_type == 'node') {
      $form['its_enabled'][$entity_type]['#description'] = t('You have to enable multilingual support to use ITS in node bundles. Bundles with no multilingual support are not shown here.');
    }
  }

  $form['its_default_data'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default translation meta data'),
    '#description' => t('This setting are used if there are no options set in the node type or directly in the node.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
  );

  $form['its_default_data'] += its_get_subform();

  $form['#submit'] = array('its_admin_form_submit');

  return system_settings_form($form);
}

/**
 * Submit handler for its_admin_form().
 *
 * Creates ITS fields for enabled bundles.
 */
function its_admin_form_submit($form, $form_state) {
  foreach ($form_state['values']['its_enabled'] as $entity_type => $bundles) {
    foreach ($bundles as $bundle_key => $value) {
      if ($value) {
        if ($entity_type == 'node' && !locale_multilingual_node_type($bundle_key)) {
          continue;
        }
        its_add_its_fields($entity_type, $bundle_key);
      }
    }
  }
  $old_values = variable_get('its_default_data', '');
  foreach ($form_state['values']['its_default_data'] as $key => $value) {
    if ($old_values[$key] != $value) {
      $field_info = field_info_field('its_' . $key);
      foreach ($field_info['bundles'] as $entity_type => $bundles) {
        foreach ($bundles as $bundle) {
          $instance = field_info_instance($entity_type, 'its_' . $key, $bundle);
          if ($instance['settings']['its_site_defaults']) {
            $instance['default_value'] = array(array('its' => $value));
            field_update_instance($instance);
          }
        }
      }
    }
  }
}
