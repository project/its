(function ($) {

  $.parseITS();

  Drupal.its = {

    /**
     * Return whether the passed node belongs to this plugin.
     *
     * @param node
     *   The currently focused DOM element in the editor content.
     * @param meta_name
     *   Name of the data category
     */
    isNode:function (node, meta_name) {
      return Drupal.its.filter.isNode(node, meta_name);
    },

    /**
     * Common invoke function.
     * Opens modal and add/edit/removes data category correctly.
     * Insert it to the text area.
     */
    invoke: function(data, settings, instanceId, meta_name) {
      var $element, cb;
      if (data.content != '') {
        var content = '<span>' + data.content + '</span>';
        $element = $(content);
        cb = function(type, action, $element) {
          if (action != 'close') {
            Drupal.wysiwyg.instances[instanceId].insert(' ' + $('<div></div>').append($element).html());
          }
          // wrap selections adds a span, this messes up saved xpaths
          $.clearITSCache();
        }
      }
      else {
        $element = $(data.node);
        cb = function() {
          // wrap selections adds a span, this messes up saved xpaths
          $.clearITSCache();
        };
      }

      Drupal.its.filter.defineITSData(meta_name, $element, cb)
    },

    /**
     * Common attach function.
     * Adds the given class to the given selector and returns the html content
     * @param content
     * @param meta_name
     * @return string
     */
    attach: function (content, meta_name) {
      var $content = $('<div>' + content + '</div>');
      $(Drupal.its.filter.selectorMap[meta_name], $content).addClass(meta_name);
      return $content.html()
    },

    /**
     * Common detach function.
     * Removes the class from the given class name and removes the class, if empty
     * @param content
     * @param meta_name
     * @return string
     */
    detach:function (content, meta_name) {
      var $content = $('<div>' + content + '</div>');
      var items = $("." + meta_name, $content);
      items.removeClass(meta_name);
      for (var i = 0; i < items.length; i++) {
        var item = $(items[i]);
        if (!item.attr('class')) {
          item.removeAttr('class');
        }
      }
      return $content.html();
    }

  };

  /**
   * Wysiwyg plugin button implementation for ITS text analysis plugin.
   */
  Drupal.wysiwyg.plugins.its_textAnalysis = {

    isNode:function (node) {
      return Drupal.its.isNode(node, 'its_textAnalysis');
    },

    invoke: function (data, settings, instanceId) {
      Drupal.its.invoke(data, settings, instanceId, 'its_textAnalysis');
    },

    attach:function (content, settings, instanceId) {
      return Drupal.its.attach(content, 'its_textAnalysis');
    },

    detach:function (content, settings, instanceId) {
      return Drupal.its.detach(content, 'its_textAnalysis');
    }

  };

  /**
   * Wysiwyg plugin button implementation for ITS locale plugin.
   */
  Drupal.wysiwyg.plugins.its_locale = {

    isNode:function (node) {
      return Drupal.its.isNode(node, 'its_locale');
    },

    invoke:function (data, settings, instanceId) {
      Drupal.its.invoke(data, settings, instanceId, 'its_locale');
    },

    attach:function (content, settings, instanceId) {
      return Drupal.its.attach(content, 'its_locale');
    },

    detach:function (content, settings, instanceId) {
      return Drupal.its.detach(content, 'its_locale');
    }

  };

  /**
   * Wysiwyg plugin button implementation for ITS locNote plugin.
   */
  Drupal.wysiwyg.plugins.its_locNote = {

    isNode:function (node) {
      return Drupal.its.isNode(node, 'its_locNote');
    },

    invoke:function (data, settings, instanceId) {
      Drupal.its.invoke(data, settings, instanceId, 'its_locNote');
    },

    attach:function (content, settings, instanceId) {
      return Drupal.its.attach(content, 'its_locNote');
    },

    detach:function (content, settings, instanceId) {
      return Drupal.its.detach(content, 'its_locNote');
    }

  };

  /**
   * Wysiwyg plugin button implementation for ITS translate plugin.
   */
  Drupal.wysiwyg.plugins.its_translate = {

    isNode:function (node) {
      return Drupal.its.isNode(node, 'its_translate');
    },

    invoke:function (data, settings, instanceId) {
      Drupal.its.invoke(data, settings, instanceId, 'its_translate');
    },

    attach:function (content, settings, instanceId) {
      return Drupal.its.attach(content, 'its_translate');
    },

    detach:function (content, settings, instanceId) {
      return Drupal.its.detach(content, 'its_translate');
    }

  };

  /**
   * Wysiwyg plugin button implementation for ITS translate plugin.
   */
  Drupal.wysiwyg.plugins.its_lang = {

    langSwitcher: false,

    isNode:function (node) {
      return Drupal.its.isNode(node, 'its_lang');
    },

    invoke:function (data, settings, instanceId) {
      Drupal.its.invoke(data, settings, instanceId, 'its_lang');
    },

    attach:function (content, settings, instanceId) {
      this.langSwitcher = $('#edit-language');
      if (this.langSwitcher.length > 0) {
        Drupal.its.filter.selectorMap['its_lang'] = ':lang(!' + this.langSwitcher.val() + ')';
        if (!Drupal.settings.its) {
          Drupal.settings.its = {};
        }
        Drupal.settings.its.language = this.langSwitcher.val();
      }
      return Drupal.its.attach(content, 'its_lang');
    },

    detach:function (content, settings, instanceId) {
      return Drupal.its.detach(content, 'its_lang');
    }


  };

  /**
   * Wysiwyg plugin button implementation for ITS translate plugin.
   */
  Drupal.wysiwyg.plugins.its_dir = {

    isNode:function (node) {
      return Drupal.its.isNode(node, 'its_dir');
    },

    invoke:function (data, settings, instanceId) {
      Drupal.its.invoke(data, settings, instanceId, 'its_dir');
    },

    attach:function (content, settings, instanceId) {
      var its = $('html').getITSData();
      Drupal.its.filter.selectorMap['its_dir'] = ':dir(!' + its.dir + ')';
      Drupal.settings.its.dir = its.dir;
      return Drupal.its.attach(content, 'its_dir');
    },

    detach:function (content, settings, instanceId) {
      return Drupal.its.detach(content, 'its_dir');
    }

  };

  /**
   * Wysiwyg plugin button implementation for ITS translate plugin.
   */
  Drupal.wysiwyg.plugins.its_terminology = {

    isNode:function (node) {
      return Drupal.its.isNode(node, 'its_terminology');
    },

    invoke:function (data, settings, instanceId) {
      Drupal.its.invoke(data, settings, instanceId, 'its_terminology');
    },

    attach:function (content, settings, instanceId) {
      return Drupal.its.attach(content, 'its_terminology');
    },

    detach:function (content, settings, instanceId) {
      return Drupal.its.detach(content, 'its_terminology');
    }

  };


}(jQuery));
