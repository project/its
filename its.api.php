<?php

/**
 * @file
 * Documents API functions for ITS Integration module.
 */

/**
 * Alter available ITS meta data categories or add new ones.
 *
 * With this function you can create new ITS fields or alter existing ones.
 * The function is used to define the input form and the database format.
 *
 * Get the description of all data categories as a drupal form element.
 *
 * @param array $data_categories
 *   Already defined data categories as a form array.
 *   The key is used as machine-readable name for the data category.
 *   The value is another array, which uses FAPI structur to define
 *   the input form. You MUST set a title, as this is also used on
 *   other places to show a readable name of the data category.
 *   As a new element is added "#cardinality" to define, if a field
 *   can have multiple values. The "#type" defines the database type:
 *     'textarea'  => text
 *     'textfield' => varchar
 *   See the example below for further information.
 */
function hook_its_meta_data_information_alter(&$data_categories) {
  // Some examples of the currently defined data categories.
  $data_categories = array(
    // Domain data category.
    'domain' => array(
      // Title of the data category.
      '#title' => t('Domain'),
      // Description of the input form element.
      '#description' => t('Specifies the domain of the text.'),
      // You can add any other possible FAPI elements here.
      // Defines indirectly the database type.
      // Used for the input form for the field.
      '#type' => 'textfield',
      // Special field: This field can have only one value.
      // Allowed values:
      // -1 => can have unlimited amount of values.
      // 1 => only one value is allowed (Default if omitted).
      // any number => number of values are allowed.
      '#cardinality' => 1,
    ),

    // Translate data category.
    'translate' => array(
      // Title of the data category.
      '#title' => t('Translate'),
      // Description of the input form element.
      '#description' => t('Specifiy parts which should be translated.'),
      // You can use 'container' or 'fieldset', if you need more than
      // one input form per field. The database will then have additional
      // columns.
      '#type' => 'fieldset',
      'selector' => array(
        '#type' => 'textfield',
        '#title' => t('Translate Selector'),
        '#description' => t('Specify which parts should be translated.'),
      ),
      'value' => array(
        '#type' => 'select',
        '#title' => t('Translate Value'),
        '#description' => t('The value of the selector.'),
        '#empty_option' => t('- Select -'),
        '#empty_value' => '',
        '#options' => array(
          'yes' => t('yes'),
          'no' => t('no'),
        ),
      ),
      // Special field: This field can have only one value.
      // Allowed values:
      // -1 => can have unlimited amount of values.
      // 1 => only one value is allowed (Default if omitted).
      // any number => number of values are allowed.
      '#cardinality' => -1,
    ),
  );
}
