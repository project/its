<?php
/**
 * @file
 * File for the Language Management Form, where the user can change all
 * ITS2.0 Properties for a node.
 */


/**
 * Callback for node/%node/its.
 *
 * Show the text and ITS meta data and allows to change this
 * via Javascript.
 *
 * @param string $entity_type
 *   the type of entity to open the language management tab
 * @param stdClass $entity
 *   current entity object
 *
 * @return string
 *   rendered form
 */
function its_lm_page($entity_type, $entity) {
  // Ensure the entity type is valid.
  if (empty($entity_type)) {
    return MENU_NOT_FOUND;
  }

  $entity_info = entity_get_info($entity_type);
  if (!$entity_info) {
    return MENU_NOT_FOUND;
  }

  if (empty($entity)) {
    return MENU_NOT_FOUND;
  }

  // Ensure access to update this particular field is granted.
  if (!entity_access('update', $entity_type, $entity)) {
    return MENU_ACCESS_DENIED;
  }

  drupal_add_css(drupal_get_path('module', 'its') . '/its.lm.css', array('group' => CSS_DEFAULT));

  $script_loaded = TRUE;

  // Load Simpletip library.
  if (($library = libraries_load('jquery.simpletip')) && empty($library['loaded'])) {
    $script_loaded = FALSE;
    if (!empty($library['error message'])) {
      drupal_set_message($library['error message'], 'error');
    }
  }

  // Load ITS-Parser library. Try first the minified variant.
  if (($library = libraries_load('jquery.its-parser')) && empty($library['loaded'])) {
    if (($library = libraries_load('jquery.its-parser', 'minified')) && empty($library['loaded'])) {
      $script_loaded = FALSE;
      if (!empty($library['error message'])) {
        drupal_set_message($library['error message'], 'error');
      }
    }
  }

  if ($script_loaded) {
    drupal_add_js(drupal_get_path('module', 'its') . '/its.lm.js', 'file');
    drupal_add_js(drupal_get_path('module', 'its') . '/its.wrapSelection.js', 'file');
  }
  else {
    if (user_access('administer modules')) {
      drupal_set_message(t('This display needs the jQuery Plugins SimpleTip and ITS Parser. Please install both libraries into the sites/all/libraries folder.'), 'error');
    }
    else {
      drupal_set_message(t('Missing jQuery Plugins. The functions on this site will not work, until this is fixed. Please inform the administrator of the page.'), 'error');
      watchdog('its', 'This display needs the jQuery Plugins SimpleTip and ITS Parser. Please install both libraries into the sites/all/libraries folder.', array(), WATCHDOG_CRITICAL, url('node/' . $entity->nid . '/its'));
    }
  }

  $form = drupal_get_form('its_lm_form', $entity_type, $entity);
  return $form;
}

/**
 * Language Management form to add and edit ITS data.
 *
 * @param string $entity_type
 *   Entity type of the entity you want to edit.
 * @param stdClass $entity
 *   The entity you want to edit.
 *
 * @return array
 *   Returns the form array for the language management form.
 */
function its_lm_form($form, &$form_state, $entity_type, $entity) {

  $langcode = entity_language($entity_type, $entity);
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);

  $form['download_files'] = array(
    '#type' => 'fieldset',
    '#title' => t('Formats'),
    '#attributes' => array('class' => array('download-files')),
    '#weight' => 1,
  );

  $form['download_files']['html-rules'] = array(
    '#markup' => l(t('HTML5 (ITS-Rules)'), 'its/' . $entity_type . '/' . $id . '/rules.xml'),
  );

  $form['download_files']['html5'] = array(
    '#markup' => l(t('HTML5 Content Only'), 'node/' . $id . '/its_html5'),
  );

  if (module_exists('linguaserve_translator')) {
    $form['download_files']['linguaserve'] = array(
      '#markup' => l(t('LinguaServe XHTML'), 'node/' . $id . '/its_lingua'),
    );
  }

  $form['global_metadata'] = array(
    '#type' => 'fieldset',
    '#title' => t('Global annotations for Translator'),
    '#attributes' => array('class' => array('meta-data-global')),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 2,
  );

  // Global meta data.
  $meta_data = its_get_meta_data_from_entity($entity, $entity_type);
  $meta_data_info = its_get_meta_data_information();
  $tmp_form = array();
  field_attach_form($entity_type, $entity, $tmp_form, $form_state, $langcode);

  foreach (array('#entity', '#entity_type', '#bundle', '#attached') as $key) {
    if (isset($tmp_form[$key])) {
      $form[$key] = $tmp_form[$key];
    }
  }
  $form['#pre_render'] = array('_field_extra_fields_pre_render');

  $domain_as_taxonomy = its_domain_taxonomy_field($entity_type, $bundle);

  // @todo: can we use its_get_subform here?
  $added = FALSE;
  foreach ($meta_data_info as $meta_name => $value) {
    if (!empty($tmp_form['its_' . $meta_name])) {
      if ($meta_name == 'domain' &&  $domain_as_taxonomy && !empty($tmp_form[$domain_as_taxonomy])) {
        $field_name = $domain_as_taxonomy;
      }
      else {
        $field_name = 'its_' . $meta_name;
      }
      $form['global_metadata'][$field_name] = $tmp_form[$field_name];
      unset($form['global_metadata'][$field_name]['#itsData']);
      $added = TRUE;
    }
  }
  if (!$added) {
    $form['global_metadata']['#type'] = 'value';
  }

  $form['metadata'] = array(
    '#type' => 'fieldset',
    '#title' => t('Annotations for Translator'),
    '#attributes' => array('class' => array('meta-data')),
    '#collapsible' => FALSE,
    '#weight' => 3,
    '#suffix' => '<div id="its-tooltip-container"></div>',
  );

  require_once 'plugins/its.inc';
  $plugins = its_its_plugin();
  $links = array();
  // Local meta data.
  foreach ($plugins as $key => $plugin) {
    if (isset($plugin['lm title'])) {
      $title = $plugin['lm title'];
    }
    else {
      $title = check_plain(str_replace('International Tag Set ', '', $plugin['title']));
    }
    if (isset($plugin['lm description'])) {
      $alt = $plugin['lm description'];
    }
    else {
      $alt = $plugin['icon title'];
    }
    if (!empty($plugin['key map'])) {
      $alt .= ' (' . $plugin['key map'] . ')';
    }
    $form['metadata']['checkbox_' . $key] = array(
      '#type' => 'checkbox',
      '#title' => l(
        t($title) . ' ' .
        theme('image', array(
          'alt' => $alt,
          'title' => $alt,
          'path' => $plugin['icon path'] . '/' . $plugin['icon file']
          )),
        '',
        array(
          'fragment' => $key,
          'external' => TRUE,
          'html' => TRUE,
          'attributes' => array(
            'title' => $alt,
          )
        )
      ),
      '#return_value' => $key,
    );
    $options = array(
      'fragment' => $key,
      'external' => TRUE,
      'html' => TRUE,
    );
    if (!empty($plugin['key map'])) {
      $options['attributes'] = array('data-key-map' => $plugin['key map']);
    }
    $links[] = l(
      theme('image', array(
          'alt' => t($title) . ' - ' . $alt,
          'title' => t($title) . ' - ' . $alt,
          'path' => $plugin['icon path'] . '/' . $plugin['icon file'])
      ), '', $options);
  }

  $form['metadata_overlay'] = array(
    '#markup' => '<div id="metadata_overlay">' . implode(' ', $links) . '</div>',
  );

  drupal_add_js(array('its' => array('language' => $form['#entity']->language)), array('type' => 'setting'));

  $form['title'] = array(
    '#type'    => 'item',
    '#title'   => t('Title'),
    '#markup'   => check_plain($entity->title),
    '#weight'  => 6,
  );

  $storage_allowed_addition = "";
  if (!empty($meta_data['allowed_characters']['node_title'])) {
    $storage_allowed_addition[] = 'Allowed Characters: ' . $meta_data['allowed_characters']['node_title'];
  }
  if (!empty($meta_data['storage_size']['node_title'])) {
    $storage_allowed_addition[] = 'Storage Size: ' . $meta_data['storage_size']['node_title'];
  }
  if (!empty($storage_allowed_addition)) {
    $form['title']['#title'] = $form['title']['#title'] . ' (' . implode(', ', $storage_allowed_addition) . ')';
  }

  $fields = field_info_instances($entity_type, $bundle);

  $form['view'] = array(
    '#type'   => 'container',
    '#attributes' => array(
      'class' => array('field-its-edit'),
    ),
    '#weight' => 7,
  );

  $form['edit'] = array(
    '#type'   => 'container',
    '#attributes' => array(
      'class' => array('field-its-edit-forms', 'element-invisible'),
    ),
  );

  $editable_fields = array();
  foreach ($fields as $field_name => $field) {
    $field_lang = field_language($entity_type, $entity, $field_name, $langcode);
    if (
      // Should not be a data category.
      (!empty($form['global_metadata'][$field_name])) ||
      // And not the domain field.
      ($domain_as_taxonomy !== 0 && $domain_as_taxonomy == $field_name) ||
      // We should have edit access.
      !field_access('edit', $field_name, $entity_type, $entity) ||
      // Only allowing it for textareas, all others goes through check_plain.
      !in_array($field['widget']['type'], array('text_textarea', 'text_textarea_with_summary')) ||
      // If it isn't defined for the language, we don't need to have it here.
      empty($entity->{$field_name}[$field_lang])
    ) {
      continue;
    }

    foreach ($entity->{$field_name}[$field_lang] as $delta => $values) {

      $storage_allowed_addition = "";
      if (!empty($meta_data['allowed_characters'][$field_name])) {
        $storage_allowed_addition[] = 'Allowed Characters: ' . $meta_data['allowed_characters'][$field_name];
      }
      if (!empty($meta_data['storage_size'][$field_name])) {
        $storage_allowed_addition[] = 'Storage Size: ' . $meta_data['storage_size'][$field_name];
      }
      if (!empty($storage_allowed_addition)) {
        $storage_allowed_addition = ' (' . implode(', ', $storage_allowed_addition) . ')';
      }

      $form['edit'][$field_name] = array(
        $field_lang => array($delta => array()),
        '#tree' => TRUE,
      );

      foreach ($values as $name => $value) {
        if (strpos($name, 'safe_') === 0) {
          continue;
        }
        if (empty($value) || $name == 'format') {
          $form['edit'][$field_name][$field_lang][$delta][$name] = array(
            '#type'   => 'value',
            '#value' => $value,
          );
        }
        else {
          $form['view'][$field_name . '-' . $field_lang . '-' . $delta . '-' . $name . '-view'] = array(
            '#type'   => 'item',
            '#title'  => $field['label'] . ' ' . $name . $storage_allowed_addition,
            '#markup' => '<div class="field-its-edit-text">' . $value . '</div>',
          );

          $form['edit'][$field_name][$field_lang][$delta][$name] = array(
            '#type'   => 'textarea',
            '#title'  => $field['label'] . ' ' . $name . $storage_allowed_addition,
            '#default_value' => $value,
          );
          $editable_fields[] = "$field_name][$field_lang][$delta][$name";
        }
      }
    }
  }

  $form['editable_fields'] = array(
    '#type'  => 'value',
    '#value' => $editable_fields,
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['save'] = array(
    '#type'   => 'submit',
    '#value'  => t('Save'),
    '#weight' => 8,
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function its_lm_form_validate($form, &$form_state) {
  $entity_type = $form_state['build_info']['args'][0];
  $entity = $form_state['build_info']['args'][1];

  // Get old values.
  $items = array();
  foreach ($form_state['values']['editable_fields'] as $key => $field_keys) {
    $field_keys = explode('][', $field_keys);
    $field_name = array_shift($field_keys);
    $items[$key] = drupal_array_get_nested_value($entity->{$field_name}, $field_keys);
  }

  field_attach_form_validate($entity_type, $entity, $form, $form_state);

  // Check against new values.
  foreach ($form_state['values']['editable_fields'] as $key => $field_keys) {
    $field_keys = explode('][', $field_keys);
    $field_name = array_shift($field_keys);
    $new_item = drupal_array_get_nested_value($entity->{$field_name}, $field_keys);
    $new_item = str_replace('&nbsp;', ' ', $new_item);
    $items[$key] = str_replace('&nbsp;', ' ', $items[$key]);
    if (strip_tags($new_item) !== strip_tags($items[$key])) {
      $form_key = $field_name . '-' . implode('-', $field_keys) . '-view';
      form_set_error($form_key, t('An error occurred. It is not possible to change content here.'));
      watchdog('its', 'Content change in translation management detected. Only malicious user can do this there.', array(), WATCHDOG_ALERT);
    }
  }
}

/**
 * Implements hook_form_submit().
 */
function its_lm_form_submit($form, &$form_state) {
  $entity_type = $form_state['build_info']['args'][0];
  $entity = $form_state['build_info']['args'][1];
  field_attach_submit($entity_type, $entity, $form, $form_state);
  entity_save($entity_type, $entity);
}

/**
 * Export a node to a specific file format output.
 *
 * @param stdClass $node
 *   node object to export
 * @param string $format
 *   format which should be used to export
 */
function its_lm_format($node, $format) {
  if (!module_exists('tmgmt')) {
    drupal_not_found();
  }
  $formatter = NULL;
  switch ($format) {
    case 'html5':
      require_once 'format/its.format.html5.inc';
      $formatter = new TMGMTFileformatHtml5Its2();
      break;

    case 'linguaserve':
      if (module_exists('linguaserve_translator')) {
        module_load_include('inc', 'linguaserve_translator', 'linguaserve_translator.format.XHTML');
        $formatter = new TMGMTFileformatLinguaServeXHTML();
        drupal_add_http_header('Content-Type', 'application/xml');
      }
      else {
        drupal_not_found();
      }
      break;

    default:
      drupal_not_found();
      return;
  }

  $job = new TMGMTJob(array('source_language' => 'en', 'target_language' => 'de'));
  $item = $job->addItem('node', 'node', $node->nid);

  print $formatter->export($job);

  $item->delete();
  $job->delete();
}
