<?php
/**
 * @file
 * Theme file for HTML5 file exporter class.
 */
?>
<!DOCTYPE html>
<html lang="<?php echo $source_language; ?>" xmlns:its="http://www.w3.org/2005/11/its">
<head>
  <meta charset="utf-8"/>
  <?php echo implode("\n", $its_meta); ?>
  <?php if (!empty($its_rules)) : ?>
  <link rel="its-rules" href="<?php echo $its_rules; ?>"><?php endif; ?>

  <title>Job ID:<?php echo $tjid; ?></title>
</head>
<body>
<?php foreach ($items as $item_key => $item): ?>
<div
  id="asset-<?php echo $item_key; ?>" <?php if (!empty($item['#its']['#addtional_attributes'])) : echo drupal_attributes($item['#its']['#addtional_attributes']); endif; ?> >
  <?php echo $item['#text']; ?>
</div>
  <?php endforeach; ?>
</body>
</html>
